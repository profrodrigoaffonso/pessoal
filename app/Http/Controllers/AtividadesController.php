<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\AtividadeTipo;
use App\Models\Marcacao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AtividadesController extends Controller
{
    public function index()
    {
        $atividades = Atividade::select('atividades.id', 'atividades.nome', 'atividade_tipos.nome AS atividade_tipo', 'atividades.distancia_volta')
                                    ->join('atividade_tipos', 'atividades.atividade_tipo_id', '=', 'atividade_tipos.id')
                                    ->orderBy('nome', 'ASC')
                                    ->paginate(10);

        return view('atividades.index', compact('atividades'));
    }

    public function create()
    {
        $atividade_tipos = AtividadeTipo::combo();

        return view('atividades.create', compact('atividade_tipos'));
    }

    public function store(Request $request)
    {
        $dados = $request->all();

        $dados['user_id'] = Auth::user()->id;

        Atividade::create($dados);

        return redirect(route('admin.atividade.index'));
    }

    public function executar($id)
    {
        $atividade = Atividade::select('atividades.id', 'atividades.nome', 'atividade_tipos.nome AS atividade_tipo', 'atividades.distancia_volta',
                                        'atividades.created_at')
                                ->join('atividade_tipos', 'atividades.atividade_tipo_id', '=', 'atividade_tipos.id')
                                ->where('atividades.id', $id)
                                ->orderBy('atividades.id', 'DESC')
                                ->first();


        $marcacoes = Marcacao::select('created_at')->where('atividade_id', $id)->orderBy('id', 'DESC')->get()->toArray();

        $info = "";

        if(count($marcacoes) > 0){
            $quantidade = count($marcacoes) - 1;

            $firstDate  = new \DateTime($marcacoes[0]['created_at']);
            $secondDate = new \DateTime(end($marcacoes)['created_at']);
            $intvl = $firstDate->diff($secondDate);

            $segundos = $intvl->i * 60 + $intvl->s;
            if($segundos > 0){
                $velocidade = $quantidade * $atividade->distancia_volta / $segundos * 3.6;
            } else {
                $velocidade = 0;
            }


            $info = "Distância: " . $quantidade * $atividade->distancia_volta ." m -
                    Tempo: " . $intvl->h . "h" . $intvl->i . " -
                    Velocidade: " . number_format($velocidade, 2, ',', '.') . " km/h";
        }

        return view('atividades.executar', compact('atividade', 'marcacoes', 'info'));

   }

   public function marcar(Request $request)
   {
       $dados = $request->all();

       Marcacao::create($dados);

       return redirect(route('executar', [$dados['atividade_id']]));


   }
}
