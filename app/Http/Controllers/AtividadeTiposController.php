<?php

namespace App\Http\Controllers;

use App\Models\AtividadeTipo;
use Illuminate\Http\Request;

class AtividadeTiposController extends Controller
{

    public function index()
    {
        $atividade_tipos = AtividadeTipo::select('id', 'nome')
                                            ->orderBy('nome', 'ASC')
                                            ->paginate(10);

        return view('atividade_tipos.index', compact('atividade_tipos'));
    }

    public function create()
    {
        return view('atividade_tipos.create');
    }

    public function store(Request $request)
    {
        $dados = $request->all();

        AtividadeTipo::create($dados);

        return redirect(route('admin.atividade.tipo.index'));

    }
}
