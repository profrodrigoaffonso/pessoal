@extends('layouts.admin')

@section('content')
<h3>Atividade - nova</h3>
<form action="{{ route('admin.atividade.store') }}" method="POST" autocomplete="off">
    @csrf
    @component('components.forms.select',[
        'label'     => 'Tipo',
        'name'      => 'atividade_tipo_id',
        'id'        => 'atividade_tipo_id',
        'values'    => $atividade_tipos,
        'selected'  => '',
        'required'  => 'required',
    ])
    @endcomponent
    @component('components.forms.input',[
        'label'     => 'Nome',
        'name'      => 'nome',
        'id'        => 'nome',
        'value'     => '',
        'maxlength' => 200,
        'required'  => 'required',
    ])
    @endcomponent
    @component('components.forms.input',[
        'label'     => 'Distância da volta',
        'name'      => 'distancia_volta',
        'id'        => 'distancia_volta',
        'value'     => '',
        'maxlength' => 10,
        'required'  => 'required',
    ])
    @endcomponent
    <button class="btn btn-success btn-sm" title="Salvar"><i data-feather="save"></i></button>
  </form>
@endsection
