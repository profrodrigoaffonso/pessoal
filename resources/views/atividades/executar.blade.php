@extends('layouts.app')

@section('content')
    <?php
    $quantidade = count($marcacoes) - 1;
    ?>
    <h3>Atividade: {{ $atividade->nome }} - {{ date('d/m/Y H:i', strtotime($atividade->created_at)) }}</h3>
    {{ $info }}
    <form method="post" action="{{ route('executar-marcar') }}">
        @csrf
        @component('components.forms.hidden', [
            'id'    => 'atividade_id',
            'name'  => 'atividade_id',
            'value' => $atividade->id
        ])
        @endcomponent
        <br>
            <button style="width: 100%" type="submit" class="btn btn-lg btn-primary">Marcar</button>
        <br>
    </form>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Marca</th>
                <th scope="col">Data e hora</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $count = 0;
            ?>
            @foreach($marcacoes as $marcacao)

            <tr>
                <td>{{ $quantidade }}</td>
                <td>{{ date('d/m/Y H:i:s', strtotime($marcacao['created_at'])) }}</td>
            </tr>
            <?php
            $quantidade--;
            ?>
            @endforeach
        </tbody>
    </table>

@endsection
