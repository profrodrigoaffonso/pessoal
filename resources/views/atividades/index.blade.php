@extends('layouts.admin')

@section('content')
    <h3>Atividades</h3>
    <p><a class="btn btn-primary btn-sm" href="{{ route('admin.atividade.create') }}"><i data-feather="plus"></i></a></p>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Tipo</th>
                <th scope="col">Distância volta</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($atividades as $atividade)
            <tr>
                <td>{{ $atividade->nome }}</td>
                <td>{{ $atividade->atividade_tipo }}</td>
                <td>{{ $atividade->distancia_volta }}</td>
                <td>
                    {{-- <a class="btn btn-primary btn-sm" href="{{ route('remedios.edit', ['id' => $atividade->id]) }}"><i data-feather="edit"></i></a> --}}
                    <a class="btn btn-primary btn-sm" href="{{ route('executar', ['id' => $atividade->id]) }}"><i data-feather="edit"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection
