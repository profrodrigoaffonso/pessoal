@extends('layouts.admin')

@section('content')
<h3>Tipo de Atividade - nova</h3>
<form action="{{ route('admin.atividade.tipo.store') }}" method="POST">
    @csrf
    @component('components.forms.input',[
        'label'     => 'Nome',
        'name'      => 'nome',
        'id'        => 'nome',
        'value'     => '',
        'maxlength' => 200,
        'required'  => 'required',
    ])
    @endcomponent
    <button class="btn btn-success btn-sm" title="Salvar"><i data-feather="save"></i></button>
  </form>
@endsection
