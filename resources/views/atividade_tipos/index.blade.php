@extends('layouts.admin')

@section('content')
    <h3>Tipo de atividades</h3>
    <p><a class="btn btn-primary btn-sm" href="{{ route('remedios.create') }}"><i data-feather="plus"></i></a></p>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($atividade_tipos as $atividade_tipo)
            <tr>
                <td>{{ $atividade_tipo->nome }}</td>
                <td>
                    <a class="btn btn-primary btn-sm" href="{{ route('remedios.edit', ['id' => $atividade_tipo->id]) }}"><i data-feather="edit"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
