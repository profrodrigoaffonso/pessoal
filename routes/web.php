<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

$path = 'App\Http\Controllers\\';

Route::get('/', function () {
    return view('auth.login');
});

Route::post('/login', 'App\Http\Controllers\LoginController@authenticate')->name('login.login');
Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::get('/executar/{id}', $path . 'AtividadesController@executar')->name('executar');
Route::post('/marcar', $path . 'AtividadesController@marcar')->name('executar-marcar');

Route::get('/receber', 'App\Http\Controllers\ComandosController@receber')->name('comandos.receber');
Route::get('/enviar_comando', 'App\Http\Controllers\ComandosController@enviarComando')->name('comandos.enviar');
Route::post('/alterar_comando', 'App\Http\Controllers\ComandosController@alterarComando')->name('comandos.executar');

Route::prefix('admin')->middleware('auth')->group(function() use ($path){
    Route::get('/', function(){
        return view('admin');
    })->name('login.admin');

    Route::prefix('fotos')->group(function(){
        Route::get('/', 'App\Http\Controllers\FotosController@index')->name('fotos.index');
        Route::get('create', 'App\Http\Controllers\FotosController@create')->name('fotos.create');
        Route::post('store', 'App\Http\Controllers\FotosController@store')->name('fotos.store');
        Route::delete('delete', 'App\Http\Controllers\FotosController@delete')->name('fotos.delete');
    });

    Route::prefix('remedios')->group(function(){
        Route::get('/', 'App\Http\Controllers\RemediosController@index')->name('remedios.index');
        Route::get('/create', 'App\Http\Controllers\RemediosController@create')->name('remedios.create');
        Route::post('/store', 'App\Http\Controllers\RemediosController@store')->name('remedios.store');
        Route::get('/{id}/edit', 'App\Http\Controllers\RemediosController@edit')->name('remedios.edit');
        Route::put('/update', 'App\Http\Controllers\RemediosController@update')->name('remedios.update');
        Route::get('/horarios', 'App\Http\Controllers\RemediosController@horarios')->name('remedios.horarios');
        Route::post('/horarios-store', 'App\Http\Controllers\RemediosController@horariosStore')->name('remedios.horarios.store');
        Route::delete('/delete-horarios', 'App\Http\Controllers\RemediosController@horariosDelete')->name('remedios.horarios.delete');
    });

    Route::prefix('atividades')->group(function () use ($path) {

        Route::get('/', $path . 'AtividadesController@index')->name('admin.atividade.index');
        Route::get('create', $path . 'AtividadesController@create')->name('admin.atividade.create');
        Route::post('store', $path . 'AtividadesController@store')->name('admin.atividade.store');

        Route::prefix('tipo')->group(function () use ($path) {
            Route::get('/', $path . 'AtividadeTiposController@index')->name('admin.atividade.tipo.index');
            Route::get('create', $path . 'AtividadeTiposController@create')->name('admin.atividade.tipo.create');
            Route::post('store', $path . 'AtividadeTiposController@store')->name('admin.atividade.tipo.store');
        });
    });
});
